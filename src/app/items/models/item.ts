export interface Item {
    Id: string;
    Name: string;
    ItemCategory:string;
    DefaultPriceConcessionName:string;
  }