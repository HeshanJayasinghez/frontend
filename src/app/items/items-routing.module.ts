import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { ItemDetailsComponent } from './item-details/item-details.component';
const routes: Routes = [
  {
    path: '',
    component: ItemDetailsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes),MatAutocompleteModule],
  exports: [RouterModule]
})
export class PriceRoutingModule { }