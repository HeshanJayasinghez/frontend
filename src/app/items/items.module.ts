import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ItemDetailsComponent } from './item-details/item-details.component';
import { PriceRoutingModule } from './items-routing.module';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {A11yModule} from '@angular/cdk/a11y';
import {ClipboardModule} from '@angular/cdk/clipboard';
import {DragDropModule} from '@angular/cdk/drag-drop';
import {CdkStepperModule} from '@angular/cdk/stepper';
import {CdkTableModule} from '@angular/cdk/table';
import {CdkTreeModule} from '@angular/cdk/tree';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { SearchItemComponent } from './search-item/search-item.component';
@NgModule({
  declarations: [ItemDetailsComponent, SearchItemComponent],
  imports: [
    A11yModule,
    ClipboardModule,
    CdkStepperModule,
    CdkTableModule,
    CdkTreeModule,
    DragDropModule,
    MatAutocompleteModule,
    CommonModule,
    PriceRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    MatInputModule
   
  ]
})
export class ItemsModule { }
