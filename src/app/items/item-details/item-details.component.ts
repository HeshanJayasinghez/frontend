import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Item } from '../models/item';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import { ItemApi } from '../Api/ItemsApi';
type NewType = Observable<Item[]>;

@Component({
  selector: 'app-price-details',
  templateUrl: './item-details.component.html',
  styleUrls: ['./item-details.component.scss']
})
export class ItemDetailsComponent implements OnInit {
  myControl = new FormControl();
  options: Item[] =[];
  filteredOptions: NewType;

  constructor(private itemApi:ItemApi){

  }
  async ngOnInit() {

    await this.itemApi.getItems().subscribe(result=>{
      
      this.options=result.Data;
      debugger;
    });

    this.filteredOptions = this.myControl.valueChanges
      .pipe(
        startWith(''),
        map(value => typeof value === 'string' ? value : value.Name),
        map(name => name ? this._filter(name) : this.options.slice())
      );
  }

  displayFn(user: Item): string {
    return user && user.Name ? user.Name : '';
  }

  private _filter(name: string): Item[] {
    debugger;
    const filterValue = name.toLowerCase();

    return this.options.filter(option => option.Name.toLowerCase().includes(filterValue));
  }

}


