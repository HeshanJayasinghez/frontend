import { HttpClient } from "@angular/common/http";

import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})

export class ItemApi {

url="http://145.239.206.89/Interview/api/test/items/?fields=id,name,description,ItemCategory,DefaultPriceConcessionID,DefaultPriceConcessionName,active";
constructor(private http: HttpClient) { }
getItems() {
    return this.http.get<any>(this.url);
  }

}