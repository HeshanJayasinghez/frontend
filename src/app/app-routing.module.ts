import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
 { path: '',
  loadChildren: () => import('./Items/items.module').then(m => m.ItemsModule)
 },
 { path: 'items',
  loadChildren: () => import('./Items/items.module').then(m => m.ItemsModule)
 }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
